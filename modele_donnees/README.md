## Direct restore
The file `bambin_pgdump.zip` contains a postgresql archive saved in plain text with --columns-insert option. Can be restored in command line by using :  
```
cd yourFolderContainingPsqlExecutable
psql -h yourHost -p yourPort -U yourUser
create database yourDbName ;
\q
psql -h yourHost -p yourPort -U yourUser -d yourDbName -f completePathOfTheFilebambin_pgdump.zip
```
## Step by step creation

### Requirements
- postgres
- postgis extension
- tsm_system_rows extension


### Folder description 

- The folder `data/` contains csv or txt files to create enumeration tables. To be used together with the sql files, it should be renamed as `datas_perso/` and copied-pasted to your postgres folder `your_path/PostgreSQL/your_version/data`.  
- The file `creation_MPD.sql` creates the (main structure of the) database by using files in `data/`.   
- The file `creation_JDDTest.sql` fills in the database (created with `creation_MPD.sql`) with random values. This requires to create first the schema `temporaire`. It also uses files in `data/`.  
- The file `schema_temporaire.sql` can be used to restore the schema   `temporaire`.  
- The files `diagramme_classe.svg` and `diagramme_ERD.svg` are the class and ERD diagrams used to build the database.  

### Provisional comments on `creation_JDDTest.sql`
- Until now, I've made "minimal" modifications so as 1) to include Auriane's corrections and 2) to remain as close as possible to the initial version of   `creation_JDDTest.sql` (to avoid mistakes).
- The main difficulty for me was to include the new date attributes like `date_heure_fin`. I'm not going to cover all cases here (better to check again!) but roughly speaking, I made it as simple as possible by considering that `date_heure_fin` of a given measurement is equal to  `date_heure_debut` of the next measurement. In `emission.icpe`, I took `date_heure_debut_obs=mesure_bruit.date_debut` and `date_heure_fin_obs=mesure_bruit.date_fin`.
- I'm not at all convinced by my modifications in `emission.trafic_fer`. Should be checked.
- In the table `emission.trafic_routier`, I don't understand (already in the initial version) the link between the values of `indicateur` and the choice of `date_heure_debut` (and now also `date_heure_fin`).
- In the table `mesure_bruit.resultat_mesure_bruit`, I set `debut_periode=Null` and `fin_periode=Null` when `is_periode_reference=true`. But I guess this should be modified by giving the actual reference periode e.g. 6h-22h.
- Note that in the table `mesure_bruit.mesure_bruit`, there are only fixed points (e.g. with `is_point_fixe=true`). It was already the case in the initial version of the file.
- Note there are weird entries (`Whisky`, `Yankee`) in the table `emission.enum_sens_circu` . Not yet corrected.



