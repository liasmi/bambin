CREATE EXTENSION IF NOT EXISTS postgis;

create schema emission ; 
create schema propagation ; 
create schema mesure_bruit ; 
create schema si ;



CREATE TABLE emission."enum_materiau_prot_acou" (
  "id_materiau_prot_acou" serial,
  "materiau_prot_acou" varchar NOT NULL,
  PRIMARY KEY ("id_materiau_prot_acou")
);
INSERT INTO emission."enum_materiau_prot_acou" (materiau_prot_acou) values 
('Terre'),
('Béton'),
('Métal'),
('Béton'),
('Bois'),
('Laine minérale'),
('Plastique transparent'),
('Verre feuilleté'),
('Autre ou inconnu');
COMMENT ON TABLE emission."enum_materiau_prot_acou" IS 'Identique à la base Plamade au 15-03-2023' ; 

CREATE TABLE mesure_bruit."enum_nature_mesure" (
  "id_nature_mesure" serial,
  "nature_mesure" varchar NOT NULL,
  PRIMARY KEY ("id_nature_mesure")
);
INSERT INTO mesure_bruit."enum_nature_mesure"(nature_mesure) VALUES
('constat'),
('long terme'),
('long terme météo') ;
COMMENT ON TABLE mesure_bruit."enum_nature_mesure" IS 'La nature des mesures est surtout citée dans les normes NF S31-085 et NF S31-088. Dépend de la connaissance des sources et des projections à horizon';

CREATE TABLE emission."enum_indicateur_trafic_routier" (
  "id_indicateur_trafic_routier" serial,
  "indicateur_trafic_routier" varchar NOT NULL,
  PRIMARY KEY ("id_indicateur_trafic_routier")
);
INSERT INTO emission."enum_indicateur_trafic_routier"(indicateur_trafic_routier) VALUES 
('TMJA'),
('pourcentage de PL annuel'),
('TMJO'),
('pourcentage de PL jours ouvrés'),
('TMJE'),
('pourcentage de PL été'),
('TMJHE'),
('pourcentage de PL hors été'),
('débit VL'),
('débit PL'),
('débit TV'),
('vitesse moyenne VL'),
('vitesse moyenne PL'),
('vitesse moyenne TV'),
('V85 VL'),
('V85 PL'),
('V85 TV'), 
('TMJ'),
('pourcentage de PL');
COMMENT ON TABLE emission."enum_indicateur_trafic_routier" IS 'Liste non finie des principaux indicateurs fournis par les gestionnniares et utiles en acoustique environnementale' ;

CREATE TABLE propagation."enum_nature_sol" (
  "id_nature_sol" serial,
  "nature_sol" varchar NOT NULL,
  PRIMARY KEY ("id_nature_sol")
) ;
INSERT INTO propagation."enum_nature_sol"(nature_sol) VALUES 
('Neige fraîche'),
('Sous-bois sec (feuilles, épines) culture'),
('Prairies, terres fraîchement labourées'),
('Gazon, terrain de stade'),
('Terre compactée, terre roulée et déchaumée'),
('Revêtements routiers fermés'),
('Eau, glace, bétons lisses et peints'),
('autre');
COMMENT ON TABLE propagation."enum_nature_sol" IS 'Liste non finie des principales natures de sol décrites par la norme Nf S31-110 au 16-03-2023' ;

CREATE TABLE emission."enum_commune" (
  "id_commune" serial,
  typecom text,
  com text NOT NULL,
  reg text,
  dep text,
  ctcd text,
  arr text,
  tncc text,
  ncc text,
  nccenr text,
  "commune" varchar NOT NULL,
  can text,
  comparent text,
  PRIMARY KEY ("id_commune")
) ; 
COPY emission."enum_commune" (typecom, com, reg, dep, ctcd, arr, tncc, ncc, nccenr, commune, can, comparent) from '/var/lib/postgresql/data/datas_perso/v_commune_2023.csv' CSV DELIMITER ',' HEADER; -- la TABLE doit etre dans le repertoire data
COMMENT ON TABLE emission."enum_commune" IS '�num�ration des communes en France, � voir si possible par API de https://api.gouv.fr/les-api/api-geo' ;

CREATE TABLE emission."enum_avion_famille" (
  "id_avion_famille" serial,
  "avion_famille" varchar NOT NULL,
  PRIMARY KEY ("id_avion_famille")
);
INSERT INTO emission."enum_avion_famille" (avion_famille) values 
('famille 1'),
('famille 2'),
('famille 3');
COMMENT ON TABLE emission."enum_avion_famille" IS 'Énumération des familles d''avion, à voir avec la DGAC' ;

CREATE TABLE emission."enum_type_source_bruit" (
  "id_type_source_bruit" serial,
  "type_source_bruit" varchar NOT NULL,
  PRIMARY KEY ("id_type_source_bruit")
) ;
INSERT INTO emission."enum_type_source_bruit"(type_source_bruit) VALUES 
('bruit routier'),
('bruit ferroviaire'),
('bruit industriel'),
('bruit aéroportuaire');
COMMENT ON TABLE emission."enum_type_source_bruit" IS 'Énumération des 4 sources de bruit considérées par le présent modèle' ;

CREATE TABLE emission."source_de_bruit" (
  "id_source_de_bruit" serial,
  "type_source_bruit" int4 NOT NULL,
  commentaire TEXT,
  PRIMARY KEY ("id_source_de_bruit"),
  CONSTRAINT "FK_source_de_bruit.type_source_bruit"
    FOREIGN KEY ("type_source_bruit")
      REFERENCES emission."enum_type_source_bruit"("id_type_source_bruit")
)
;
COMMENT ON TABLE emission."source_de_bruit" IS 'Une source de bruit est relative soit au trafic routier, ferrovaiire, aérien ou à un fonctionnement d''ICPE' ;

CREATE TABLE mesure_bruit."campagne_mesure" (
  "id_campagne_mesure" serial,
  "description" text NOT NULL,
  PRIMARY KEY ("id_campagne_mesure")
);
COMMENT ON TABLE mesure_bruit."campagne_mesure" IS 'Une campagne permet de mettre en relation plusieurs mesures liées au même objectif' ;

CREATE TABLE mesure_bruit."enum_type_materiel" (
  "id_type_materiel" serial,
  "type_materiel" varchar NOT null,
  PRIMARY KEY ("id_type_materiel")
);
INSERT INTO mesure_bruit."enum_type_materiel"(type_materiel) VALUES 
('sonomètre classe 1'),
('sonomètre classe 2'),
('chaine de mesure personalisée'),
('smartphone');
COMMENT ON TABLE mesure_bruit."enum_type_materiel" IS 'Énumération des grande catégorie de matérile de mesure de bruit' ;

CREATE TABLE mesure_bruit."enum_marque_materiel" (
  "id_marque_materiel" serial,
  "marque_materiel" varchar NOT NULL,
  PRIMARY KEY ("id_marque_materiel")
);
INSERT INTO mesure_bruit."enum_marque_materiel"(marque_materiel) VALUES 
('ACOEM'),
('Bruel et Kjaer'),
('Samsung'),
('Apple'),
('Nokia'),
('autre');
COMMENT ON TABLE mesure_bruit."enum_marque_materiel" IS 'Énumération des grande marque de matérile de mesure de bruit' ;

CREATE TABLE mesure_bruit."materiel_mesure" (
  "id_materiel_mesure" serial,
  "id_type_materiel" int4 NOT NULL DEFAULT 1,
  "id_marque_materiel" int4,
  "numero_serie" varchar,
  PRIMARY KEY ("id_materiel_mesure"),
  CONSTRAINT "FK_materiel_mesure.id_type_materiel"
    FOREIGN KEY ("id_type_materiel")
      REFERENCES mesure_bruit."enum_type_materiel"("id_type_materiel"),
  CONSTRAINT "FK_materiel_mesure.id_marque_materiel"
    FOREIGN KEY ("id_marque_materiel")
      REFERENCES mesure_bruit."enum_marque_materiel"("id_marque_materiel"),
  UNIQUE (id_marque_materiel, numero_serie),
  CONSTRAINT materiel_mesure_num_serie_sans_marque_KO CHECK ((numero_serie IS NOT NULL AND id_marque_materiel IS NOT null) OR numero_serie IS NULL) 
);
COMMENT ON TABLE mesure_bruit."materiel_mesure" IS 'Un matériel est un objet réel. Son type est obligatoirement connu' ;

CREATE TABLE mesure_bruit."enum_piece_mesure" (
  "id_piece_mesure" serial,
  "piece_mesure" varchar NOT NULL,
  PRIMARY KEY ("id_piece_mesure")
);
INSERT INTO mesure_bruit."enum_piece_mesure"(piece_mesure) VALUES 
('salon '),
('chambre'),
('salle à manger'),
('cuisine'),
('salle de bain'),
('autre');
COMMENT ON TABLE mesure_bruit."enum_piece_mesure" IS 'Une piece peut faire l''objet de mesure en cas de SOURCE de TYPE ICPE interne à un bâtiment' ;


CREATE TABLE mesure_bruit."rapport" (
  "id_rapport" serial,
  "titre" text NOT NULL,
  "url_rapport" text NOT NULL,
  PRIMARY KEY ("id_rapport")
);
COMMENT ON TABLE mesure_bruit."rapport" IS 'Table de stockage des documents' ;


CREATE TABLE mesure_bruit."localisation_mesure" (
  "id_localisation_mesure" serial,
  "geom" geometry(point, 2154) NOT NULL,
  commentaire TEXT,
  PRIMARY KEY ("id_localisation_mesure")
);
COMMENT ON TABLE mesure_bruit."localisation_mesure" IS 'Géolocalisation d''une ou plusieurs mesure en RGF93 Lambert 93' ;


CREATE TABLE mesure_bruit."enum_piece_mesure_orient" (
  "id_piece_mesure_orient" serial,
  "piece_mesure_orient" varchar NOT NULL,
  PRIMARY KEY ("id_piece_mesure_orient")
);
INSERT INTO mesure_bruit."enum_piece_mesure_orient"(piece_mesure_orient) VALUES 
('Nord '),
('Sud'),
('Est '),
('Ouest'),
('Nord-Est'),
('Nord-Ouest'),
('Sud-Est'),
('Sud-Ouest '),
('autre');
COMMENT ON TABLE mesure_bruit."enum_piece_mesure_orient" IS 'Orientation selon les points cardinaux' ;


CREATE TABLE propagation."meteo" (
  "id_meteo" serial,
  "is_conventionnelle" bool NOT NULL DEFAULT True,
  "is_quantitatif" bool NOT NULL,
  "is_station" bool NOT NULL,
  commentaire TEXT,
  PRIMARY KEY ("id_meteo")
);
COMMENT ON TABLE propagation."meteo" IS 'Description des conditions de recueil de la météo lors d''une mesure' ;

CREATE TABLE "mesure_bruit"."mesure_bruit" (
  "id_mesure_bruit" serial,
  "maitre_ouvrage" varchar NOT NULL,
  "maitre_oeuvre" varchar,
  "is_point_fixe" bool NOT NULL DEFAULT True,
  id_point_fixe int4,
  "id_localisation_mesure" int4 NOT NULL,
  "is_exterieur" bool NOT NULL DEFAULT True,
  "is_facade" bool DEFAULT True,
  "is_champ" bool DEFAULT False,
  "hauteur_mesure" float NOT NULL,
  "id_piece_mesure" int4,
  "id_piece_mesure_orient" int4,
  "is_ponctuel" bool NOT NULL DEFAULT True,
  "date_debut" Timestamptz NOT NULL,
  "date_fin" Timestamptz NOT NULL,
  "duree_integration" float NOT NULL DEFAULT 1,
  "id_materiel_mesure" int4 NOT NULL,
  "id_rapport" int4,
  "id_campagne_mesure" int4,
  "id_meteo" int4,
  commentaire TEXT,
  PRIMARY KEY ("id_mesure_bruit"),
  CONSTRAINT "FK_mesure_bruit.id_campagne_mesure"
    FOREIGN KEY ("id_campagne_mesure")
      REFERENCES mesure_bruit."campagne_mesure"("id_campagne_mesure"),
  CONSTRAINT "FK_mesure_bruit.id_materiel_mesure"
    FOREIGN KEY ("id_materiel_mesure")
      REFERENCES mesure_bruit."materiel_mesure"("id_materiel_mesure"),
  CONSTRAINT "FK_mesure_bruit.id_piece_mesure"
    FOREIGN KEY ("id_piece_mesure")
      REFERENCES mesure_bruit."enum_piece_mesure"("id_piece_mesure"),
  CONSTRAINT "FK_mesure_bruit.id_rapport"
    FOREIGN KEY ("id_rapport")
      REFERENCES mesure_bruit."rapport"("id_rapport"),
  CONSTRAINT "FK_mesure_bruit.id_localisation_mesure"
    FOREIGN KEY ("id_localisation_mesure")
      REFERENCES mesure_bruit."localisation_mesure"("id_localisation_mesure"),
  CONSTRAINT "FK_mesure_bruit.id_piece_mesure_orient"
    FOREIGN KEY ("id_piece_mesure_orient")
      REFERENCES mesure_bruit."enum_piece_mesure_orient"("id_piece_mesure_orient"),
  CONSTRAINT "FK_mesure_bruit.id_point_fixe"
    FOREIGN KEY ("id_point_fixe")
      REFERENCES mesure_bruit.mesure_bruit("id_mesure_bruit"),
  CONSTRAINT "FK_mesure_bruit.id_meteo"
    FOREIGN KEY ("id_meteo")
      REFERENCES propagation."meteo"("id_meteo"),
  CONSTRAINT date_ordonnees CHECK (date_debut < date_fin),
  CONSTRAINT exterieur_exterieur_type_null_non_null CHECK (is_exterieur = true AND is_facade IS NOT null AND is_champ IS null),
  CONSTRAINT interieur_id_piece_mesure_null_not_null CHECK ((is_exterieur = false AND id_piece_mesure IS NOT NULL AND id_piece_mesure_orient IS not NULL) OR 
                                                       (is_exterieur = true AND id_piece_mesure IS NULL AND id_piece_mesure_orient IS null)),
  CONSTRAINT prelevement_ref_point_fixe CHECK (is_point_fixe = true OR (is_point_fixe=false AND id_point_fixe IS NOT null)),
  CONSTRAINT mesure_bruit_hauteur_sup_0 CHECK (hauteur_mesure > 0),
  CONSTRAINT mesure_bruit_duree_integration_sup_0 CHECK (duree_integration > 0)
);
COMMENT ON TABLE "mesure_bruit"."mesure_bruit" IS 'Caractéristiques d''une mesure de bruit : localisation, parties prenantes, météo, matériel... . La durée d''intégration est en seconde' ;
--Ajout d'une contrainte de vérification pour les mesures à géométrie partagée
 CREATE OR REPLACE FUNCTION mesure_bruit.check_mesure_id_loc_partage()
  RETURNS BOOlEAN AS $$
 DECLARE verif boolean ; 
 BEGIN 
 	verif = (
 	WITH 
 		groupe AS (
 		SELECT id_localisation_mesure, is_exterieur, is_facade, is_champ, id_piece_mesure, id_piece_mesure_orient, count(*) over(PARTITION BY id_localisation_mesure) nb_id_localisation
 		 FROM mesure_bruit.mesure_bruit), 		
	 	verif_params as (
		SELECT id_localisation_mesure,
		       count(DISTINCT is_exterieur) nb_id_bati_type,
		       count(DISTINCT is_facade) nb_facade,
		       count(DISTINCT is_champ) nb_champ,
		       count(DISTINCT id_piece_mesure) nb_id_piece_mesure,
		       count(DISTINCT id_piece_mesure_orient) nb_id_piece_mesure_orient
		 FROM groupe
		 WHERE nb_id_localisation > 1
		 GROUP BY id_localisation_mesure)
		select EXISTS (SELECT 
		 FROM verif_params
		 WHERE 1 < any(ARRAY[nb_id_bati_type, nb_facade, nb_champ, nb_id_piece_mesure, nb_id_piece_mesure_orient]))) ;
	RETURN verif ;
 END ;
 $$
LANGUAGE plpgsql ;
COMMENT ON FUNCTION mesure_bruit.check_mesure_id_loc_partage() IS 'verifier que les mesures partageant un mm id_localisation ont bien les mêmes caractéritisques de type de bati, in_out, piece_mesure et orient_piece_mesure' ;
ALTER TABLE mesure_bruit.mesure_bruit ADD CONSTRAINT mesure_bruit_check_mesure_id_loc_partage CHECK (mesure_bruit.check_mesure_id_loc_partage() = False) ;

CREATE TABLE mesure_bruit."relation_source_mesure" (
  "id_source_mesure" serial,
  "id_source_de_bruit" int4 NOT NULL,
  "id_mesure_bruit" int4 NOT NULL,
  PRIMARY KEY ("id_source_mesure"),
  CONSTRAINT "FK_relation_source_mesure.id_source_de_bruit"
    FOREIGN KEY ("id_source_de_bruit")
      REFERENCES emission."source_de_bruit"("id_source_de_bruit"),
  CONSTRAINT "FK_relation_source_mesure.id_mesure_bruit"
    FOREIGN KEY ("id_mesure_bruit")
      REFERENCES "mesure_bruit"."mesure_bruit"("id_mesure_bruit"),
  UNIQUE (id_source_de_bruit, id_mesure_bruit)
);
COMMENT ON TABLE mesure_bruit."relation_source_mesure" IS 'Lien entre une mesure de bruit et une ou plusieurs sources' ;

CREATE TABLE emission."enum_hygro_rvt_chausse" (
  "id_hygro_rvt" serial,
  "hygro_rvt" varchar NOT NULL,
  PRIMARY KEY ("id_hygro_rvt")
);
INSERT INTO emission."enum_hygro_rvt_chausse"(hygro_rvt) VALUES
('sèche'),
('détrempée') ;
COMMENT ON TABLE emission."enum_hygro_rvt_chausse" IS 'caractère sec ou détrempée du revetement de chaussée';

CREATE TABLE emission."enum_rvt_granulo" (
  "id_rvt_granulo" serial,
  "rvt_granulo" varchar NOT NULL,
  PRIMARY KEY ("id_rvt_granulo")
);
INSERT INTO emission."enum_rvt_granulo"(rvt_granulo) VALUES
('0/10'),
('0/6'),
('0/14'),
('0/10 classe2'),
('0/10 classe1'),
('0/4'),
('0/6 classe1'),
('0/6 classe2'),
('0/8 classe1'),
('0/8'),
('0,8/1,5'),
('10/14'),
('4/6'),
('6/10'),
('6/8'),
('8/10') ;
COMMENT ON TABLE emission."enum_rvt_granulo" IS 'granulométrie des revetement de chaussée présent dans la Bdd des revetement de l''UMRAE en février 2023';

CREATE TABLE emission."enum_rvt_etat" (
  "id_rvt_etat" serial,
  "rvt_etat" varchar NOT NULL,
  PRIMARY KEY ("id_rvt_etat")
);
INSERT INTO emission."enum_rvt_etat"(rvt_etat) VALUES
('normal'),
('orniérage'),
('détritus'),
('autre') ;
COMMENT ON TABLE emission."enum_rvt_etat" IS 'état de surface du revetement de chaussée';

CREATE TABLE emission."enum_rvt_nature" (
  "id_rvt_nature" serial,
  "rvt_nature" varchar NOT NULL,
  PRIMARY KEY ("id_rvt_nature")
);
INSERT INTO emission."enum_rvt_nature"(rvt_nature) VALUES
('ECF'),
('BBDr'),
('BBM'),
('BBS'),
('BBSG'),
('BBTM'),
('BBUM'),
('BC'),
('Colgrip'),
('MBCF'),
('Enduit'),
('ESU'),
('Lisse (résine)'),
('Sable enrobe'),
('Thermo'),
('SMA10'),
('RSC'),
('Bicouche') ;
COMMENT ON TABLE emission."enum_rvt_nature" IS 'Nature  des revetements de chaussée présent dans la Bdd des revetement de l''UMRAE en février 2023';

CREATE TABLE emission."enum_dept" (
  "id_dept" serial,
  dept varchar(3) NOT null,
  reg text,
  cheflieu text,
  tncc text,
  ncc text,
  nccenr text,
  LIBELLE text,
  PRIMARY KEY ("id_dept")
);
COPY emission."enum_dept" (dept, reg, cheflieu, tncc, ncc, nccenr, LIBELLE) from '/var/lib/postgresql/data/datas_perso/v_departement_2023.csv' CSV DELIMITER ',' HEADER ; -- la TABLE doit etre dans le repertoire data
COMMENT ON TABLE emission."enum_dept" IS '�num�ration des d�partements en France, � voir si possible par API de https://api.gouv.fr/les-api/api-geo';

CREATE TABLE emission."enum_gestionnaire" (
  "id_gest_route" serial,
  "gest_route" varchar NOT NULL,
  PRIMARY KEY ("id_gest_route")
);
COPY emission."enum_gestionnaire"(gest_route) FROM '/var/lib/postgresql/data/datas_perso/valeur_route_gestionnaire.txt' ;
COMMENT ON TABLE emission."enum_gestionnaire" IS 'Liste des gestionnaires routiers issue des routes num�rot�e ou nomm�e de la BdTopo';

CREATE TABLE emission."enum_nom_route" (
  "id_nom_route" serial,
  "nom_route" varchar NOT NULL,
  PRIMARY KEY ("id_nom_route")
);
COPY emission.enum_nom_route(nom_route) FROM '/var/lib/postgresql/data/datas_perso/valeur_route.csv' CSV DELIMITER ',' ;
COMMENT ON TABLE emission."enum_nom_route" IS 'Liste des noms de route issus de https://adresse.data.gouv.fr/data/ban/adresses/latest/csv/ uniquement le 33 pour les noms de rue, et de flux WFS BdTopo pour les routes num�rot�es sur la france entiere ';

CREATE TABLE emission."route" (
  "id_route" serial,
  "id_nom_route" int4 NOT NULL,
  "id_gest_route" int4,
  "id_dept" int4 NOT NULL,
  "id_commune" int4,
  PRIMARY KEY ("id_route"),
  CONSTRAINT "FK_route.id_dept"
    FOREIGN KEY ("id_dept")
      REFERENCES emission."enum_dept"("id_dept"),
  CONSTRAINT "FK_route.id_commune"
    FOREIGN KEY ("id_commune")
      REFERENCES emission."enum_commune"("id_commune"),
  CONSTRAINT "FK_route.id_gest_route"
    FOREIGN KEY ("id_gest_route")
      REFERENCES emission."enum_gestionnaire"("id_gest_route"),
  CONSTRAINT "FK_route.id_nom_route"
    FOREIGN KEY ("id_nom_route")
      REFERENCES emission."enum_nom_route"("id_nom_route"),
  unique (id_nom_route, id_gest_route, id_dept, id_commune)
);
COMMENT ON TABLE emission."route" IS 'Une route est simplement décrite par ses caracéristiques de nom, gestionnaire et département, plus commune encas de voie communale' ;

CREATE TABLE emission."enum_sens_circu" (
  "id_sens_circu" serial,
  "sens_circu" varchar NOT NULL,
  PRIMARY KEY ("id_sens_circu")
);
INSERT INTO emission."enum_sens_circu"(sens_circu) VALUES
('Nord'),
('Sud'),
('Est'),
('Ouest'),
('Nord-Est'),
('Nord-Ouest'),
('Sud-Est'),
('Sud-Ouest'),
('Intérieur'),
('Extérieur'),
('Whisky'),
('Yankee'),
('Paris'),
('Province'),
('autre'),
('double sens');
COMMENT ON TABLE emission."enum_sens_circu" IS 'sens de circulation, pour route ou voie ferrée';

CREATE TABLE emission."infra_route" (
  "id_infra_route" serial,
  "id_route" int4 NOT NULL,
  "largeur" float,
  "nb_voie" int4,
  "declivite" float,
  "sens_circulation" int4,
  "id_rvt_nature" int4,
  "id_rvt_etat" int4,
  "id_rvt_granulo" int4,
  "is_rvt_sec" bool,
  PRIMARY KEY ("id_infra_route"),
  CONSTRAINT "FK_infra_route.id_rvt_granulo"
    FOREIGN KEY ("id_rvt_granulo")
      REFERENCES emission."enum_rvt_granulo"("id_rvt_granulo"),
  CONSTRAINT "FK_infra_route.id_rvt_etat"
    FOREIGN KEY ("id_rvt_etat")
      REFERENCES emission."enum_rvt_etat"("id_rvt_etat"),
  CONSTRAINT "FK_infra_route.id_rvt_nature"
    FOREIGN KEY ("id_rvt_nature")
      REFERENCES emission."enum_rvt_nature"("id_rvt_nature"),
  CONSTRAINT "FK_infra_route.id_route"
    FOREIGN KEY ("id_route")
      REFERENCES emission."route"("id_route"),
  CONSTRAINT "FK_trafic_routier.sens_circulation"
    FOREIGN KEY ("sens_circulation")
      REFERENCES emission."enum_sens_circu"("id_sens_circu"),
  CONSTRAINT infra_route_largeur_sup_0 CHECK (largeur > 0), 
  CONSTRAINT infra_route_nb_voie_sup_0 CHECK (nb_voie > 0)
);
COMMENT ON TABLE emission."infra_route" IS 'L''infra est forcément liée à une route. Elle décrit les caractéristiques géométriquesdes tronçons concernés' ;

CREATE TABLE propagation."enum_etat_neige" (
  "id_etat_neige" serial,
  "etat_neige" varchar NOT NULL,
  PRIMARY KEY ("id_etat_neige")
);
INSERT INTO propagation."enum_etat_neige"(etat_neige) VALUES
('fraiche'),
('transformée'),
('gelée') ;
COMMENT ON TABLE propagation."enum_etat_neige" IS '';

CREATE TABLE propagation."enum_hygro_sol" (
  "id_hygro_sol" serial,
  "hygro_sol" varchar NOT NULL,
  PRIMARY KEY ("id_hygro_sol")
);
INSERT INTO propagation."enum_hygro_sol"(hygro_sol) VALUES
('humide'),
('détrempé'),
('sec'),
('gelée'),
('enneigé') ;
COMMENT ON TABLE propagation."enum_hygro_sol" IS 'Hygrométrie du sol entre les sources et le recepteur';

CREATE TABLE propagation."description_sol" (
  "id_description_sol" serial,
  "id_source_mesure" int4 NOT NULL,
  "id_nature_sol" int4 NOT NULL,
  "pourcentage_occupation" int4 NOT NULL DEFAULT 100,
  "id_hygro_sol" int4,
  "epaisseur_neige" int4,
  "etat_neige" int4,
  PRIMARY KEY ("id_description_sol"),
  CONSTRAINT "FK_description_sol.etat_neige"
    FOREIGN KEY ("etat_neige")
      REFERENCES propagation."enum_etat_neige"("id_etat_neige"),
  CONSTRAINT "FK_description_sol.id_nature_sol"
    FOREIGN KEY ("id_nature_sol")
      REFERENCES propagation."enum_nature_sol"("id_nature_sol"),
  CONSTRAINT "FK_description_sol.id_source_mesure"
    FOREIGN KEY ("id_source_mesure")
      REFERENCES mesure_bruit."relation_source_mesure"("id_source_mesure"),
  CONSTRAINT "FK_description_sol.id_hygro_sol"
    FOREIGN KEY ("id_hygro_sol")
      REFERENCES propagation."enum_hygro_sol"("id_hygro_sol"),
  CONSTRAINT description_sol_pourcentage_entre_1_et_100 CHECK (pourcentage_occupation BETWEEN 1 AND 100),
  CONSTRAINT description_sol_epaisseur_neige_sup_0_id_hygro_5 CHECK ((epaisseur_neige > 0 and id_hygro_sol = 5) OR id_hygro_sol != 5),
  UNIQUE (id_source_mesure, id_nature_sol, pourcentage_occupation)
);
COMMENT ON TABLE propagation."description_sol" IS 'Relatif a chaque couple source mesure, le sol peut etre composé de plusieurs zones, décrites par un état et une hygrométrie. Si neige, elle est décrite' ;

CREATE TABLE emission."icpe" (
  "id_icpe" serial,
  "numero_siret" varchar NOT NULL,
  "id_source_de_bruit" int4 NOT NULL,
  "description_icpe" text NOT NULL,
  "date_heure_debut_obs" timestamptz NOT NULL,
  "date_heure_fin_obs" timestamptz NOT NULL,
  "observation_duree_bruit_particulier" text,
  "observation_moment_manifestation" text,
  "observation_conditions_emission_bruit" text,
  PRIMARY KEY ("id_icpe"),
  CONSTRAINT "FK_icpe.id_source_de_bruit"
    FOREIGN KEY ("id_source_de_bruit")
      REFERENCES emission."source_de_bruit"("id_source_de_bruit"),
  UNIQUE (id_source_de_bruit, numero_siret)
);
COMMENT ON TABLE emission."icpe" IS 'Une icpe est principalement décrite par son numéro SIRET. Le reste correspond à des decsription acoustique du fonctionnement en tant que source de bruit' ;

CREATE TABLE emission."enum_avion_modele" (
  "id_avion_modele" serial,
  "avion_modele" varchar NOT NULL,
  PRIMARY KEY ("id_avion_modele")
);
INSERT INTO emission."enum_avion_modele"(avion_modele) VALUES
('gros avion'),
('petit avion');
COMMENT ON TABLE emission."enum_avion_modele" IS 'Il faudra boucler avec la DGAC ou aéroport de Bordeaux';

CREATE TABLE emission."enum_categorie_trafic_fer" (
  "id_categorie_trafic_fer" serial,
  "categorie_trafic_fer" varchar NOT NULL,
  PRIMARY KEY ("id_categorie_trafic_fer")
);
INSERT INTO emission."enum_categorie_trafic_fer"(categorie_trafic_fer) VALUES
('TGV'),
('Fret'),
('TER / Grandes lignes'),
('Metro'),
('Tram'),
('RER Francilien'),
('autre') ;
COMMENT ON TABLE emission."enum_categorie_trafic_fer" IS 'Grandes catégories de trafic ferré. issu de NF S31-088';

CREATE TABLE emission."voie_ferree" (
  "numero_ligne" varchar NOT NULL,
  "nom_commun" varchar,
  "is_lgv" bool NOT NULL,
  PRIMARY KEY ("numero_ligne")
);
COMMENT ON TABLE emission."voie_ferree" IS 'Une voie ferrée est un objet immatériel décrit par son numéro interne SNCF-Réseau, sa nature et son nom commun' ;

CREATE TABLE emission."enum_rvt_fer" (
  "id_rvt_fer" serial,
  "rvt_fer" varchar NOT NULL,
  PRIMARY KEY ("id_rvt_fer")
);
INSERT INTO emission."enum_rvt_fer"(rvt_fer) VALUES
('enrobé'),
('gazon'),
('pavé'),
('autre');
COMMENT ON TABLE emission."enum_rvt_fer" IS 'Revetement éventuel entre et autour des voies. cf norme NF S31-088';

CREATE TABLE emission."enum_rvt_rails" (
  "id_rvt_rails" serial,
  "rvt_rails" varchar NOT NULL,
  PRIMARY KEY ("id_rvt_rails")
);
INSERT INTO emission."enum_rvt_rails"(rvt_rails) VALUES
('long rails soudés'),
('rail éclissés') ;
COMMENT ON TABLE emission."enum_rvt_rails" IS 'Nature des rails. cf norme NF S31-088';

CREATE TABLE emission."enum_rvt_traverse" (
  "id_rvt_traverse" serial,
  "rvt_traverse" varchar NOT NULL,
  PRIMARY KEY ("id_rvt_traverse")
);
INSERT INTO emission."enum_rvt_traverse"(rvt_traverse) VALUES
('métal'),
('béton'),
('bois') ;
COMMENT ON TABLE emission."enum_rvt_traverse" IS 'Nature des traverses. cf norme NF S31-088';

CREATE TABLE emission."enum_point_singulier_fer" (
  "id_point_singulier_fer" serial,
  "point_singulier_fer" varchar NOT NULL,
  PRIMARY KEY ("id_point_singulier_fer")
);
INSERT INTO emission."enum_point_singulier_fer"(point_singulier_fer) VALUES
('courbe'),
('pont'),
('aiguillage') ;
COMMENT ON TABLE emission."enum_point_singulier_fer" IS 'Nature des points singuliers. cf norme NF S31-088';

CREATE TABLE emission."enum_position_sol" (
  "id_position_sol" serial,
  "position_sol" varchar NOT NULL,
  PRIMARY KEY ("id_position_sol")
);
INSERT INTO emission."enum_position_sol"(position_sol) VALUES
('au sol'),
('déblai'),
('remblai') ;
COMMENT ON TABLE emission."enum_position_sol" IS 'Position de la voie par rapport au sol. cf norme NF S31-088';

CREATE TABLE emission."enum_pose_fer" (
  "id_type_pose_fer" serial,
  "type_pose_fer" varchar NOT NULL,
  PRIMARY KEY ("id_type_pose_fer")
);
INSERT INTO emission."enum_pose_fer"(type_pose_fer) VALUES
('voie sur ballast'),
('voie sur dalle avec rails incorporés') ;
COMMENT ON TABLE emission."enum_pose_fer" IS 'type de pose. cf norme NF S31-088';

CREATE TABLE emission."infra_voie_ferree" (
  "id_infra_voie_ferree" serial,
  "numero_ligne" varchar NOT NULL,
  "declivite" float,
  "id_position_sol" int4,
  "id_rvt_fer" int4,
  "id_type_pose_fer" int4,
  "id_type_traverse" int4,
  "id_type_rail" int4,
  "id_point_singulier_fer" int4,
  PRIMARY KEY ("id_infra_voie_ferree"),
  CONSTRAINT "FK_infra_voie_ferree.numero_ligne"
    FOREIGN KEY ("numero_ligne")
      REFERENCES emission."voie_ferree"("numero_ligne"),
  CONSTRAINT "FK_infra_voie_ferree.id_rvt_fer"
    FOREIGN KEY ("id_rvt_fer")
      REFERENCES emission."enum_rvt_fer"("id_rvt_fer"),
  CONSTRAINT "FK_infra_voie_ferree.id_type_rail"
    FOREIGN KEY ("id_type_rail")
      REFERENCES emission."enum_rvt_rails"("id_rvt_rails"),
  CONSTRAINT "FK_infra_voie_ferree.id_type_traverse"
    FOREIGN KEY ("id_type_traverse")
      REFERENCES emission."enum_rvt_traverse"("id_rvt_traverse"),
  CONSTRAINT "FK_infra_voie_ferree.id_point_singulier_fer"
    FOREIGN KEY ("id_point_singulier_fer")
      REFERENCES emission."enum_point_singulier_fer"("id_point_singulier_fer"),
  CONSTRAINT "FK_infra_voie_ferree.id_position_sol"
    FOREIGN KEY ("id_position_sol")
      REFERENCES emission."enum_position_sol"("id_position_sol"),
  CONSTRAINT "FK_infra_voie_ferree.id_type_pose_fer"
    FOREIGN KEY ("id_type_pose_fer")
      REFERENCES emission."enum_pose_fer"("id_type_pose_fer")
);
COMMENT ON TABLE emission."infra_voie_ferree" IS 'Décrit les caractéristiques physiques d''une voie ferree';

CREATE TABLE emission."enum_indicateur_trafic_fer" (
  "id_indicateur_trafic_fer" serial,
  "indicateur_trafic_fer" varchar,
  PRIMARY KEY ("id_indicateur_trafic_fer")
);
insert into emission."enum_indicateur_trafic_fer"(indicateur_trafic_fer) values
('TMJA'),
('débit'),
('TMJ');
comment on table emission."enum_indicateur_trafic_fer" is 'indicateur possibe pour le fer : trafic moyen ou débit';

CREATE TABLE emission."trafic_fer" (
  "id_trafic_fer" serial,
  "id_infra_voie_ferree" int4 NOT NULL,
  "sens_circulation" int4,
  "id_source_de_bruit" int NOT NULL,
  "date_heure_debut" Timestamptz,
  "date_heure_fin" Timestamptz,
  "id_indicateur_trafic_fer" int4,
  "trafic_nature" int4,
  "vitesse" int4 NOT NULL,
  "nb_passage" int4 NOT NULL,
  PRIMARY KEY ("id_trafic_fer"),
  CONSTRAINT "FK_trafic_fer.id_infra_voie_ferree"
    FOREIGN KEY ("id_infra_voie_ferree")
      REFERENCES emission."infra_voie_ferree"("id_infra_voie_ferree"),
  constraint "FK_trafic_fer.sens_circulation"
    foreign key ("sens_circulation")
      references emission.enum_sens_circu("id_sens_circu"),
  CONSTRAINT "FK_trafic_fer.id_source_de_bruit"
    FOREIGN KEY ("id_source_de_bruit")
      REFERENCES emission."source_de_bruit"("id_source_de_bruit"),
  CONSTRAINT "FK_trafic_fer.id_indicateur_trafic_fer"
    FOREIGN KEY ("id_indicateur_trafic_fer")
      REFERENCES emission."enum_indicateur_trafic_fer"("id_indicateur_trafic_fer"),
  CONSTRAINT "FK_trafic_fer.trafic_nature"
    FOREIGN KEY ("trafic_nature")
      REFERENCES mesure_bruit."enum_nature_mesure"("id_nature_mesure"),
  UNIQUE (id_infra_voie_ferree, sens_circulation, date_heure_debut, date_heure_fin, id_indicateur_trafic_fer, trafic_nature, vitesse, nb_passage),
  CONSTRAINT materiel_roulant_vitesse_sup_0 CHECK (vitesse > 0),
  CONSTRAINT materiel_roulant_nb_passage_sup_0 CHECK (nb_passage > 0),
  CONSTRAINT horodate_nulle_si_tmj CHECK ((id_indicateur_trafic_fer IN (1,3)
  	AND date_heure_debut IS NULL) OR (id_indicateur_trafic_fer = 2 AND date_heure_debut IS NOT NULL))
);
COMMENT ON TABLE emission."trafic_fer" IS 'assure le lien entre la décomposition du trafic, l''infra et les sources de bruit';

CREATE TABLE emission."materiel_roulant" (
  "id_materiel_roulant" serial,
  "id_trafic_fer" int4 NOT NULL,
  "id_categorie_trafic_fer" int4,
  PRIMARY KEY ("id_materiel_roulant", "id_trafic_fer"),
  UNIQUE(id_materiel_roulant),
  CONSTRAINT "FK_materiel_roulant.id_categorie_trafic_fer"
    FOREIGN KEY ("id_categorie_trafic_fer")
      REFERENCES emission."enum_categorie_trafic_fer"("id_categorie_trafic_fer"),
  CONSTRAINT "FK_materiel_roulant.id_trafic_fer"
    FOREIGN KEY ("id_trafic_fer")
      REFERENCES emission."trafic_fer"("id_trafic_fer")
);
COMMENT ON TABLE emission."materiel_roulant" IS 'Décrit le type de matériel selon la catégorisation fournie par SCNF réseau';

CREATE TABLE emission."enum_type_prot_acou" (
  "id_type_prot_acou" serial,
  "type_prot_acou" varchar NOT NULL,
  PRIMARY KEY ("id_type_prot_acou")
);
INSERT INTO emission."enum_type_prot_acou"(type_prot_acou) VALUES
('écran acoustique'),
('butte de terre'),
('modelé'),
('parement'),
('couverture'),
('autre') ;
COMMENT ON TABLE emission."enum_type_prot_acou" IS 'type de protection acoustique issu du géostandard bruit dans l''environnement partie 2';

CREATE TABLE emission."enum_module_roulant" (
  "id_module_roulant" serial,
  "module_roulant" varchar NOT NULL,
  PRIMARY KEY ("id_module_roulant")
);
INSERT INTO emission."enum_module_roulant"(module_roulant) VALUES
('module1'),
('module2'),
('autre');
COMMENT ON TABLE emission."enum_module_roulant" IS 'A completer avec une source fournie par SNCF ou SNCF Réseau';


CREATE TABLE emission."arrete_exploitation" (
  "id_arrete_exploitation" serial,
  "id_icpe" int4 NOT NULL,
  "reference" varchar NOT NULL,
  "date_approbation" date NOT NULL,
  "date_abrogation" date,
  PRIMARY KEY ("id_arrete_exploitation"),
  CONSTRAINT "FK_arrete_exploitation.id_icpe"
    FOREIGN KEY ("id_icpe")
      REFERENCES emission."icpe"("id_icpe")
);
COMMENT ON TABLE emission."arrete_exploitation" IS 'Document fixant les conditions d''exploitation d''une ICPE.' ;

CREATE TABLE emission."appareil_aerien" (
  "identifiant_unique_international_avion" varchar,
  "id_avion_modele" int4,
  "id_avion_famille" int4,
  "id_motorisation" int4,
  PRIMARY KEY ("identifiant_unique_international_avion"),
  CONSTRAINT "FK_appareil_aerien.id_avion_modele"
    FOREIGN KEY ("id_avion_modele")
      REFERENCES emission."enum_avion_modele"("id_avion_modele"),
  CONSTRAINT "FK_appareil_aerien.id_avion_famille"
    FOREIGN KEY ("id_avion_famille")
      REFERENCES emission."enum_avion_famille"("id_avion_famille")
);
COMMENT ON TABLE emission."appareil_aerien" IS 'Description des caractéristiques d''un avion' ;

CREATE TABLE emission."enum_aeroport" (
  "id_aeroport" serial,
  "aeroport" varchar NOT NULL,
  PRIMARY KEY ("id_aeroport")
);
INSERT INTO emission."enum_aeroport"(aeroport) VALUES
('AGEN-LA-GARENNE'),
('AJACCIO-NAPOLEON-BONAPARTE'),
('ALBI-LE-SEQUESTRE'),
('ANGERS-MARCE'),
('ANGOULEME-BRIE-CHAMPNIERS'),
('ANNECY-MEYTHET'),
('AURILLAC'),
('AUXERRE-BRANCHES'),
('AVIGNON-CAUMONT'),
('BASTIA-PORETTA'),
('BEAUVAIS-TILLE'),
('BERGERAC-ROUMANIERE'),
('BEZIERS-VIAS'),
('BIARRITZ-BAYONNE-ANGLET'),
('BORDEAUX-MERIGNAC'),
('BOURGES'),
('BREST-BRETAGNE'),
('BRIVE-VALLEE-DE-DORDOGNE-'),
('CAEN-CARPIQUET'),
('CAHORS-LALBENQUE'),
('CALAIS-DUNKERQUE'),
('CALVI-ST-CATHERINE'),
('CANNES-MANDELIEU'),
('CARCASSONNE-SALVAZA'),
('CASTRES-MAZAMET'),
('CHALONS-VATRY'),
('CHAMBERY-AIX-LES-BAINS'),
('CHATEAUROUX-DEOLS'),
('CHERBOURG-MAUPERTUS'),
('CHOLET-LE-PONTREAU'),
('CLERMONT-FERRAND-CLERMONT-FERRAND/AUVERGNE'),
('COLMAR-HOUSSEN'),
('DEAUVILLE-ST-GATIEN'),
('DIJON-LONGVIC'),
('DINARD-PLEURTUIT-ST-MALO'),
('DOLE-TAVAUX'),
('EPINAL-MIRECOURT'),
('FIGARI-SUD-CORSE'),
('GRENOBLE-ISERE'),
('ILE-D''YEU-LE-GRAND-PHARE'),
('LA-ROCHELLE-ILE-DE-RE'),
('LANNION'),
('LAVAL-ENTRAMMES'),
('LE-HAVRE-OCTEVILLE-SUR-MER'),
('LE-MANS-ARNAGE'),
('LE-PUY-LOUDES'),
('LE-TOUQUET-PARIS-PLAGE'),
('LILLE-LESQUIN'),
('LIMOGES-BELLEGARDE'),
('LORIENT-LANN-BIHOUE'),
('LYON-BRON-BRON'),
('LYON-SAINT-EXUPERY'),
('MARSEILLE-PROVENCE'),
('MEGEVE-MEGEVE'),
('METZ-NANCY-LORRAINE'),
('MONTLUCON-GUERET'),
('MONTPELLIER-MEDITERRANEE'),
('MORLAIX-PLOUJEAN'),
('BALE-MULHOUSE'),
('NANTES-ATLANTIQUE'),
('NICE-COTE-D''AZUR'),
('NIMES-GARONS-'),
('NIORT-SOUCHE'),
('PARIS-CHARLES-DE-GAULLE'),
('PARIS-ISSY-LES-MOULINEAUX'),
('PARIS-LE-BOURGET'),
('PARIS-ORLY'),
('PAU-PYRENEES'),
('PERIGUEUX-BASSILLAC'),
('PERPIGNAN-RIVESALTES'),
('POITIERS-BIARD'),
('PROPRIANO'),
('QUIMPER-PLUGUFFAN'),
('RENNES-ST-JACQUES'),
('ROANNE-RENAISON'),
('ROCHEFORT-ST-AGNANT'),
('RODEZ-MARCILLAC'),
('ROUEN-VALLEE-DE-SEINE'),
('SAINT-BRIEUC-ARMOR'),
('SAINT-ETIENNE-BOUTHEON'),
('SAINT-NAZAIRE-MONTOIR'),
('STRASBOURG-ENTZHEIM'),
('TARBES-LOURDES-PYRENEES'),
('HYERES-LE-PALYVESTRE'),
('TOULOUSE-BLAGNAC'),
('TOURS-VAL-DE-LOIRE'),
('TROYES-BARBEREY'),
('VALENCE-CHABEUIL'),
('VALENCIENNES-DENAIN'),
('VICHY-CHARMEIL') ;
COMMENT ON TABLE emission."enum_aeroport" IS 'liste des aéroports issue de https://www.data.gouv.fr/fr/datasets/aeroports-francais-coordonnees-geographiques/#/resources';

CREATE TABLE emission."circulation_aerienne" (
  "id_circulation_aerienne" serial,
  "identifiant_unique_international_avion" varchar,
  "aeroport" int4 NOT NULL,
  "id_source_de_bruit" int4,
  "date_heure_passage" timestamptz NOT NULL,
  PRIMARY KEY ("id_circulation_aerienne"),
  CONSTRAINT "FK_circulation_aerienne.id_source_de_bruit"
    FOREIGN KEY ("id_source_de_bruit")
      REFERENCES emission."source_de_bruit"("id_source_de_bruit"),
  CONSTRAINT "FK_circulation_aerienne.aeroport"
    FOREIGN KEY ("aeroport")
      REFERENCES emission."enum_aeroport"("id_aeroport"),
  CONSTRAINT "FK_circulation_aerienne.identifiant_unique_international_avion"
    FOREIGN KEY ("identifiant_unique_international_avion")
      REFERENCES emission."appareil_aerien"("identifiant_unique_international_avion"),
  UNIQUE (id_circulation_aerienne, identifiant_unique_international_avion, aeroport, date_heure_passage)
);
COMMENT ON TABLE emission."circulation_aerienne" IS 'Caractérise le passage d''un avion à un point de l''espace etdu temps donné. Voir avec DGAC pour modèle plus adapté' ;

CREATE TABLE emission."trajectoire" (
  "id_trajectoire" serial,
  "id_circulation_aerienne" int4 NOT NULL,
  "point_passage" geometry(pointZ, 2154) NOT NULL,
  "date_heure_passage" timestamptz NOT NULL,
  PRIMARY KEY ("id_trajectoire"),
  CONSTRAINT "FK_trajectoire.id_circulation_aerienne"
    FOREIGN KEY ("id_circulation_aerienne")
      REFERENCES emission."circulation_aerienne"("id_circulation_aerienne"),
  UNIQUE (id_circulation_aerienne, date_heure_passage)
);
COMMENT ON TABLE emission."trajectoire" IS 'description d''une trajectoire d''avion par association espace - temps' ;

CREATE TABLE emission."protection_acoustique" (
  "id_prot_acou" serial,
  "geom" geometry(linestring, 2154) NOT NULL,
  "id_materiau_prot_acou" int4,
  "inclinaison" int4,
  "id_type_prot_acou" int4,
  PRIMARY KEY ("id_prot_acou"),
  CONSTRAINT "FK_protection_acoustique.id_type_prot_acou"
    FOREIGN KEY ("id_type_prot_acou")
      REFERENCES emission."enum_type_prot_acou"("id_type_prot_acou"),
  CONSTRAINT "FK_protection_acoustique.id_materiau_prot_acou"
    FOREIGN KEY ("id_materiau_prot_acou")
      REFERENCES emission."enum_materiau_prot_acou"("id_materiau_prot_acou")
);
COMMENT ON TABLE emission."protection_acoustique" IS 'Tiré du modèle de données du géostandard bruit dans l''environnement partie 2' ;

CREATE TABLE emission."association_source_prot_acou" (
  "id_association_route_prot_acou" serial,
  "id_infra_route" int4,
  "id_prot_acou" int4 NOT NULL,
  "id_infra_voie_ferree" int4,
  PRIMARY KEY ("id_association_route_prot_acou"),
  CONSTRAINT "FK_association_source_prot_acou.id_infra_voie_ferree"
    FOREIGN KEY ("id_infra_voie_ferree")
      REFERENCES emission."infra_voie_ferree"("id_infra_voie_ferree"),
  CONSTRAINT "FK_association_source_prot_acou.id_prot_acou"
    FOREIGN KEY ("id_prot_acou")
      REFERENCES emission."protection_acoustique"("id_prot_acou"),
  CONSTRAINT "FK_association_source_prot_acou.id_infra_route"
    FOREIGN KEY ("id_infra_route")
      REFERENCES emission."infra_route"("id_infra_route")
);
COMMENT ON TABLE emission."protection_acoustique" IS 'Permet d''associer une ou plusieurs protections acoustiques à une ou plusieurs routes' ;

CREATE TABLE mesure_bruit."certification_materielle" (
  "id_certif_materielle" serial,
  "debut_validite" date NOT NULL,
  "fin_validite" date,
  "url_certif_materiel" text,
  "id_materiel_mesure" int4 NOT NULL,
  PRIMARY KEY ("id_certif_materielle"),
  CONSTRAINT "FK_certification_materielle.id_materiel_mesure"
    FOREIGN KEY ("id_materiel_mesure")
      REFERENCES mesure_bruit."materiel_mesure"("id_materiel_mesure")
);
COMMENT ON TABLE mesure_bruit."certification_materielle" IS 'La certification permet de vérifgier qu''un matériel est bien validé comme conforme pour la réalisation de mesures reglementaires, à un instant T' ;

CREATE TABLE emission."trafic_routier" (
  "id_trafic_routier" serial,
  "id_infra_route" int4 NOT NULL,
  "id_source_de_bruit" int4 NOT NULL,
  "date_heure_debut" Timestamptz,
  "date_heure_fin" Timestamptz,
  "indicateur" int4,
  "valeur" float not null,
  "trafic_nature" int4,
  PRIMARY KEY ("id_trafic_routier"),
  CONSTRAINT "FK_trafic_routier.indicateur"
    FOREIGN KEY ("indicateur")
      REFERENCES emission."enum_indicateur_trafic_routier"("id_indicateur_trafic_routier"),
  CONSTRAINT "FK_trafic_routier.id_source_de_bruit"
    FOREIGN KEY ("id_source_de_bruit")
      REFERENCES emission."source_de_bruit"("id_source_de_bruit"),
  CONSTRAINT "FK_trafic_routier.id_infra_route"
    FOREIGN KEY ("id_infra_route")
      REFERENCES emission."infra_route"("id_infra_route"),
  CONSTRAINT "FK_trafic_routier.trafic_nature"
    FOREIGN KEY ("trafic_nature")
      REFERENCES mesure_bruit."enum_nature_mesure"("id_nature_mesure"),
  CONSTRAINT trafic_routier_valeur_sup_0 CHECK (valeur > 0),
  UNIQUE (id_infra_route, date_heure_debut, date_heure_fin, indicateur, trafic_nature)
);
COMMENT ON TABLE emission."trafic_routier" IS 'le trafic routiers est caractérisé par un ou plusieurs identificateur, sur une période explicite ou sur une période reglemantire. cf norme NF S31-085' ;


CREATE TABLE emission."composition_materiel_roulant" (
  "id_composition_materiel_roulant" serial,
  "id_materiel_roulant" int4,
  "id_module_roulant" int4,
  "nombre" int4,
  PRIMARY KEY ("id_composition_materiel_roulant", "id_materiel_roulant"),
  CONSTRAINT "FK_composition_materiel_roulant.id_module_roulant"
    FOREIGN KEY ("id_module_roulant")
      REFERENCES emission."enum_module_roulant"("id_module_roulant"),
  CONSTRAINT "FK_composition_materiel_roulant.id_materiel_roulant"
    FOREIGN KEY ("id_materiel_roulant")
      REFERENCES emission."materiel_roulant"("id_materiel_roulant"),
  constraint compoistion_materiel_roulant_nombre_sup_0 check (nombre > 0)
);
COMMENT ON TABLE emission."composition_materiel_roulant" IS 'description d''un type de passage de train par le nombre et le type de matériel le composant ' ;


CREATE TABLE propagation."station_meteo" (
  "id_station_meteo" serial,
  "is_station_fixe" bool NOT NULL,
  "description" varchar NOT NULL,
  "geom" geometry(point, 2154) NOT NULL,
  PRIMARY KEY ("id_station_meteo"),
  UNIQUE (is_station_fixe, description, geom)
);
COMMENT ON TABLE propagation."station_meteo" IS 'localisation et description d''une station météo';

CREATE TABLE propagation."enum_indicateur_meteo" (
  "id_indicateur_meteo" serial,
  "indicateur_meteo" varchar NOT NULL,
  PRIMARY KEY ("id_indicateur_meteo")
);
INSERT INTO propagation."enum_indicateur_meteo"(indicateur_meteo) VALUES
('température'),
('vitesse du vent'),
('direction du vent'),
('rayonnement'),
('couverture nuageuse'),
('humidité du sol'),
('heure de lever de soleil'),
('heure de coucher de soleil') ;
COMMENT ON TABLE propagation."enum_indicateur_meteo" IS 'liste des indicateurs selon la norme NF S31-120';

CREATE TABLE propagation."mesure_station_meteo" (
  "id_mesure_station" serial,
  "id_meteo" int4 NOT NULL,
  "date_heure_debut" timestamptz NOT NULL,
  "date_heure_fin" timestamptz NOT NULL,
  "id_indicateur_meteo" int4 NOT NULL,
  "hauteur" float NOT NULL,
  "valeur" float NOT NULL,
  "id_station_meteo" int4 NOT NULL,
  PRIMARY KEY ("id_mesure_station"),
  CONSTRAINT "FK_mesure_station_meteo.id_station_meteo"
    FOREIGN KEY ("id_station_meteo")
      REFERENCES propagation."station_meteo"("id_station_meteo"),
  CONSTRAINT "FK_mesure_station_meteo.id_meteo"
    FOREIGN KEY ("id_meteo")
      REFERENCES propagation."meteo"("id_meteo"),
  CONSTRAINT "FK_mesure_station_meteo.id_indicateur_meteo"
    FOREIGN KEY ("id_indicateur_meteo")
      REFERENCES propagation."enum_indicateur_meteo"("id_indicateur_meteo"),
  CONSTRAINT mesure_station_meteo_hauteur_sup_0 CHECK (hauteur > 0),
  CONSTRAINT mesure_station_meteo_valeur_sup_0_hors_temperature CHECK ((valeur > 0 AND id_indicateur_meteo > 1) OR id_indicateur_meteo = 1),
  UNIQUE (id_meteo, date_heure_debut, date_heure_fin, id_indicateur_meteo, hauteur, id_station_meteo)
);
COMMENT ON TABLE propagation."mesure_station_meteo" IS 'horodatage et valeur mesurées par les stations météo';

CREATE TABLE propagation."enum_valeur_meteo_observees" (
  "id_valeur_meteo_observees" char(2),
  "description" text NOT NULL,
  PRIMARY KEY ("id_valeur_meteo_observees")
);
INSERT INTO propagation."enum_valeur_meteo_observees"(id_valeur_meteo_observees, description) VALUES
('U1', 'vent fort contraire au sens source-récepteur '),
('U2', 'vent moyen contraire OU vent fort peu contraire OU vent moyen peu contraire'),
('U3', 'vent faible OU vent soufflant de travers'),
('U4', 'vent moyen portant OU vent fort peu portant OU vent moyen peu portant'),
('U5', 'vent fort portant'),
('T1', 'jour ET rayonnement fort ET surface du sol sèche ET (vent moyen ou faible)'),
('T2', 'jour ET conditions de T1 non satisfaites ET conditions de T3 non satisfaites'),
('T3', 'période de lever ou de lever du soleil OU (période de jour ET rayonnement moyen à faible ET surface du sol humide ET vent fort)'),
('T4', 'nuit ET conditions de T5 non satisfaites'),
('T5', 'nuit ET ciel dégagé ET vent faible') ;
COMMENT ON TABLE propagation."enum_valeur_meteo_observees" IS 'code et définition des valeurs de météo observées. cf norme NF S31-120';

CREATE TABLE propagation."observation_meteo" (
  "id_observation_meteo" serial,
  "id_meteo" int4 NOT NULL,
  "date_heure_debut" timestamptz NOT NULL,
  "date_heure_fin" timestamptz NOT NULL,
  "valeur" char(2) NOT NULL,
  PRIMARY KEY ("id_observation_meteo"),
  CONSTRAINT "FK_observation_meteo.id_meteo"
    FOREIGN KEY ("id_meteo")
      REFERENCES propagation."meteo"("id_meteo"),
  CONSTRAINT "FK_observation_meteo.valeur"
    FOREIGN KEY ("valeur")
      REFERENCES propagation."enum_valeur_meteo_observees"("id_valeur_meteo_observees"),
  UNIQUE (id_meteo, date_heure_debut, date_heure_fin, valeur)
);
COMMENT ON TABLE propagation."observation_meteo" IS 'horodatage et valeur météo observées par une personne physique';

CREATE TABLE mesure_bruit."enum_indicateur" (
  "id_indicateur" serial,
  "indicateur" varchar NOT NULL,
  PRIMARY KEY ("id_indicateur")
);
INSERT INTO mesure_bruit."enum_indicateur"(indicateur) VALUES
('Leq'),
('NA'),
('émergence'),
('Lmax'),
('Leq résiduel'),
('Leq particulier'),
('tonalité marquée'),
('SEL'),
('L50'),
('L90'),
('L10') ;
COMMENT ON TABLE mesure_bruit."enum_indicateur" IS 'liste des indicateurs de bruit. Non exhaustif';

CREATE TABLE mesure_bruit."enum_unite_grandeur" (
  "id_unite_grandeur" serial,
  "unite_grandeur" varchar NOT NULL,
  PRIMARY KEY ("id_unite_grandeur")
);
INSERT INTO mesure_bruit."enum_unite_grandeur"(unite_grandeur) VALUES
('décibel'),
('Pascal'),
('Hertz'),
('nombre d''évenement') ;
COMMENT ON TABLE mesure_bruit."enum_unite_grandeur" IS 'unité de grandeur des indicateurs bruit. Non exhaustif';

CREATE TABLE mesure_bruit."enum_ponderation" (
  "id_ponderation" serial,
  "ponderation" varchar NOT NULL,
  PRIMARY KEY ("id_ponderation")
);
INSERT INTO mesure_bruit."enum_ponderation"(ponderation) VALUES
('A'),
('B'),
('C'),
('Z') ;
COMMENT ON TABLE mesure_bruit."enum_ponderation" IS 'pondération fréquentielles. Non exhaustif';


CREATE TABLE mesure_bruit."resultat_mesure_bruit" (
  "id_resultat_mesure_bruit" serial,
  "id_mesure_bruit" int4 NOT NULL,
  "id_nature_mesure" int4 NOT NULL,
  "id_indicateur" int4 NOT NULL DEFAULT 1,
  "id_unite_grandeur" int4 NOT NULL DEFAULT 1,
  "id_ponderation" int4 NOT NULL DEFAULT 1,
  "debut_periode" timestamptz,
  "fin_periode" timestamptz,
  "is_periode_reference" bool,
  "valeur" float NOT NULL,
  "incertitude" float,
  commentaire TEXT,
  PRIMARY KEY ("id_resultat_mesure_bruit"),
  CONSTRAINT "FK_resultat_mesure_bruit.id_mesure_bruit"
    FOREIGN KEY ("id_mesure_bruit")
      REFERENCES "mesure_bruit"."mesure_bruit"("id_mesure_bruit"),
  CONSTRAINT "FK_resultat_mesure_bruit.id_indicateur"
    FOREIGN KEY ("id_indicateur")
      REFERENCES mesure_bruit."enum_indicateur"("id_indicateur"),
  CONSTRAINT "FK_resultat_mesure_bruit.id_unite_grandeur"
    FOREIGN KEY ("id_unite_grandeur")
      REFERENCES mesure_bruit."enum_unite_grandeur"("id_unite_grandeur"),
  CONSTRAINT "FK_resultat_mesure_bruit.id_ponderation"
    FOREIGN KEY ("id_ponderation")
      REFERENCES mesure_bruit."enum_ponderation"("id_ponderation"),
  CONSTRAINT "FK_resultat_mesure_bruit.id_nature_mesure"
    FOREIGN KEY ("id_nature_mesure")
      REFERENCES mesure_bruit."enum_nature_mesure"("id_nature_mesure"),
  CONSTRAINT resultat_mesure_bruit_valeur_sup_0 CHECK (valeur > 0),
  UNIQUE (id_mesure_bruit, id_nature_mesure, id_indicateur, id_ponderation, debut_periode, fin_periode, is_periode_reference)
);
COMMENT ON TABLE mesure_bruit."resultat_mesure_bruit" IS 'Valeur des indicateurs de bruit mesurés, validés par des opérateurs acoustiques. Relatifs à un intervalle de temps ou une période de référence' ;

CREATE TABLE mesure_bruit."enum_nature_bande_freq" (
  "id_nature_bande_freq" serial,
  "nature_bande_freq" varchar NOT NULL,
  PRIMARY KEY ("id_nature_bande_freq")
);
INSERT INTO mesure_bruit."enum_nature_bande_freq"(nature_bande_freq) VALUES
('octave'),
('tiers d''octave');
COMMENT ON TABLE mesure_bruit."enum_nature_bande_freq" IS 'nature de la décomposition fréquentielle';

CREATE TABLE mesure_bruit."enum_bande_freq" (
  "frequence_centrale" int4,
  PRIMARY KEY ("frequence_centrale")
);
INSERT INTO mesure_bruit."enum_bande_freq" VALUES
('25'),
('31'),
('40'),
('50'),
('63'),
('80'),
('100'),
('125'),
('160'),
('200'),
('250'),
('315'),
('400'),
('500'),
('630'),
('800'),
('1000'),
('1250'),
('1600'),
('2000'),
('2500'),
('3150'),
('4000'),
('5000'),
('6300'),
('8000'),
('10000'),
('12500'),
('16000'),
('20000') ;
COMMENT ON TABLE mesure_bruit."enum_bande_freq" IS 'valeur centrale de la bande fréquentielle, cf https://fr.wikipedia.org/wiki/Bande_d%27octave';

CREATE TABLE mesure_bruit."bruit_frequence" (
  "id_bruit_frequence" serial,
  "id_resultat_mesure_bruit" int4,
  "id_nature_bande_freq" int4 NOT NULL,
  "frequence_centrale" int4 NOT NULL,
  "niveau" float NOT NULL,
  PRIMARY KEY ("id_bruit_frequence"),
  CONSTRAINT "FK_bruit_frequence.id_nature_bande_freq"
    FOREIGN KEY ("id_nature_bande_freq")
      REFERENCES mesure_bruit."enum_nature_bande_freq"("id_nature_bande_freq"),
  CONSTRAINT "FK_bruit_frequence.frequence_centrale"
    FOREIGN KEY ("frequence_centrale")
      REFERENCES mesure_bruit."enum_bande_freq"("frequence_centrale"),
  CONSTRAINT "FK_bruit_frequence.id_resultat_mesure_bruit"
    FOREIGN KEY ("id_resultat_mesure_bruit")
      REFERENCES mesure_bruit."resultat_mesure_bruit"("id_resultat_mesure_bruit"),
  CONSTRAINT bruit_frequence_niveau_sup_0 CHECK (niveau > 0),
  CONSTRAINT verif_octave CHECK ((id_nature_bande_freq=1 AND frequence_centrale IN (31, 63, 125, 250, 500, 1000, 2000, 4000, 8000, 16000)) OR id_nature_bande_freq=2),
  UNIQUE (id_resultat_mesure_bruit, id_nature_bande_freq, frequence_centrale)
);
COMMENT ON TABLE mesure_bruit."bruit_frequence" IS 'Décomposition fréquentielle d''un résultat';


CREATE TABLE si."user" (
  "id_user" serial,
  "firstname" varchar NOT NULL,
  "lastname" varchar NOT NULL,
  "mail" varchar NOT NULL,
  "numero_siren" varchar,
  "type_source_bruit" int4,
  "is_authorized" boolean default False NOT NULL,
  PRIMARY KEY ("id_user"),
  CONSTRAINT "FK_user.type_source_bruit"
    FOREIGN KEY ("type_source_bruit")
      REFERENCES emission."enum_type_source_bruit"("id_type_source_bruit"),
  UNIQUE (mail)
);
insert into si.user(firstname, lastname, mail, numero_siren, is_authorized) 
 values ('Martin', 'Schoreisz', 'martin.schoreisz@cerema.fr', '13001831000255', True) ;
