from datetime import datetime
from pydantic import BaseModel
from typing import Optional


class MandatoryFormInput(BaseModel):
    # table mesure_bruit.mesure_bruit
    maitre_ouvrage: str
    maitre_oeuvre: Optional[str] = None
    is_point_fixe: Optional[bool] = True
    id_point_fixe: Optional[int] = None
    id_localisation_mesure: Optional[
        int
    ] = None  # table mesure_bruit.mesure_bruit & mesure_bruit.localisation_mesure ; optionnel : si on cree une géométrie il sera cree apres
    is_exterieur: Optional[bool] = True
    is_facade: Optional[bool] = True
    is_champ: Optional[bool] = False
    hauteur_mesure: float
    id_piece_mesure: Optional[int] = None
    id_piece_mesure_orient: Optional[int] = None
    is_ponctuel: Optional[bool] = True
    date_debut: datetime
    date_fin: datetime
    duree_integration: Optional[float] = 1
    id_materiel_mesure: int
    id_rapport: Optional[int] = None
    id_campagne_mesure: Optional[int] = None
    id_meteo: Optional[int] = None
    comment_mesure: Optional[str] = None

    # table mesure_bruit.localisation_mesure
    x_coordinate_epsg2154: Optional[int] = None
    y_coordinate_epsg2154: Optional[int] = None
    comment_localisation: Optional[str] = None

    # table resultats_mesure
    id_nature_mesure: Optional[int] = 1
    id_indicateur: Optional[int] = 1
    id_unite_grandeur: Optional[int] = 1
    id_ponderation: Optional[int] = 1
    debut_periode: datetime
    fin_periode: datetime
    is_periode_reference: Optional[bool] = True
    valeur: float
    incertitude: Optional[float] = None
    comment_resultat: Optional[str] = None
