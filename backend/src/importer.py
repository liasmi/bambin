# Module d'import des donnees contenues dans la base
# notamment les prochaines valeur d'identifiants

from sqlalchemy import Sequence


def nextval_id_localisation_mesure(session_instance):
    with session_instance() as session:
        id_loc_next = session.scalar(Sequence('localisation_mesure_id_localisation_mesure_seq', schema='mesure_bruit'))
    return id_loc_next


def nextval_id_mesure_bruit(session_instance):
    with session_instance() as session:
        id_loc_next = session.scalar(Sequence('mesure_bruit_id_mesure_bruit_seq', schema='mesure_bruit'))
    return id_loc_next