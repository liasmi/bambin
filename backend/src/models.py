from sqlalchemy.orm import declarative_base
from sqlalchemy import Column, Integer, String, Float, DateTime, Boolean, Sequence
from geoalchemy2 import Geometry

Base = declarative_base()


class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    fullname = Column(String)
    nickname = Column(String)

    def __repr__(self):
        return "<User(name='%s', fullname='%s', nickname='%s')>" % (
            self.name,
            self.fullname,
            self.nickname,
        )


class MesureBruit(Base):
    __tablename__ = "mesure_bruit"
    __table_args__ = {"schema": "mesure_bruit"}
    id_mesure_bruit = Column(
        Integer,
        Sequence("mesure_bruit_id_mesure_bruit_seq", schema="mesure_bruit"),
        primary_key=True,
    )
    maitre_ouvrage = Column(String, nullable=False)
    maitre_oeuvre = Column(String)
    is_point_fixe = Column(Boolean, nullable=False)
    id_point_fixe = Column(Integer)
    id_localisation_mesure = Column(Integer, nullable=False)
    is_exterieur = Column(Boolean, nullable=False)
    is_facade = Column(Boolean)
    is_champ = Column(Boolean)
    hauteur_mesure = Column(Float, nullable=False)
    id_piece_mesure = Column(Integer)
    id_piece_mesure_orient = Column(Integer)
    is_ponctuel = Column(Boolean, nullable=False)
    date_debut = Column(DateTime, nullable=False)
    date_fin = Column(DateTime, nullable=False)
    duree_integration = Column(Float, nullable=False)
    id_materiel_mesure = Column(Integer, nullable=False)
    id_rapport = Column(Integer)
    id_campagne_mesure = Column(Integer)
    id_meteo = Column(Integer)
    commentaire = Column(String)


class LocalisationMesure(Base):
    __tablename__ = "localisation_mesure"
    __table_args__ = {"schema": "mesure_bruit"}
    id_localisation_mesure = Column(
        Integer,
        Sequence(
            "localisation_mesure_id_localisation_mesure_seq", schema="mesure_bruit"
        ),
        primary_key=True,
    )
    geom = Column(Geometry("POINT"))
    commentaire = Column(String)


class MaterielMesure(Base):
    __tablename__ = "materiel_mesure"
    __table_args__ = {"schema": "mesure_bruit"}
    id_materiel_mesure = Column(
        Integer,
        Sequence("materiel_mesure_id_materiel_mesure_seq", schema="mesure_bruit"),
        primary_key=True,
    )
    id_type_materiel = Column(Integer)
    id_marque_materiel = Column(Integer)
    numero_serie = Column(String)


class enumTypeMateriel(Base):
    __tablename__ = "enum_type_materiel"
    __table_args__ = {"schema": "mesure_bruit"}
    id_type_materiel = Column(
        Integer,
        Sequence(
            "enum_type_materiel_id_type_materiel_seq", schema="mesure_bruit"
        ),
        primary_key=True,
    )
    type_materiel = Column(String)


class enumMarqueMateriel(Base):
    __tablename__ = "enum_marque_materiel"
    __table_args__ = {"schema": "mesure_bruit"}
    id_marque_materiel = Column(
        Integer,
        Sequence(
            "enum_marque_materiel_id_marque_materiel_seq", schema="mesure_bruit"
        ),
        primary_key=True,
    )
    marque_materiel = Column(String)


class ResultatMesureBruit(Base):
    __tablename__ = "resultat_mesure_bruit"
    __table_args__ = {'schema': 'mesure_bruit'}
    id_mesure_bruit = Column(Integer, Sequence('resultat_mesure_bruit_id_resultat_mesure_bruit_seq', schema='mesure_bruit'), primary_key=True)
    id_nature_mesure = Column(Integer, nullable=False)
    id_indicateur = Column(Integer, nullable=False)
    id_unite_grandeur = Column(Integer, nullable=False)
    id_ponderation = Column(Integer, nullable=False)
    debut_periode = Column(DateTime)
    fin_periode = Column(DateTime)
    is_periode_reference = Column(Boolean)
    valeur = Column(Float, nullable=False)
    incertitude = Column(Float)
    commentaire = Column(String)


class enumNatureMesure(Base):
    __tablename__ = "enum_nature_mesure"
    __table_args__ = {'schema': 'mesure_bruit'}
    id_nature_mesure = Column(Integer, Sequence('enum_nature_mesure_id_nature_mesure_seq', schema='mesure_bruit'), primary_key=True)
    nature_mesure = Column(String, nullable=False)


class enumPonderation(Base):
    __tablename__ = "enum_ponderation"
    __table_args__ = {'schema': 'mesure_bruit'}
    id_ponderation = Column(Integer, Sequence('enum_ponderation_id_ponderation_seq', schema='mesure_bruit'), primary_key=True)
    ponderation = Column(String, nullable=False)


class enumUniteGrandeur(Base):
    __tablename__ = "enum_unite_grandeur"
    __table_args__ = {'schema': 'mesure_bruit'}
    id_unite_grandeur = Column(Integer, Sequence('enum_unite_grandeur_id_unite_grandeur_seq', schema='mesure_bruit'), primary_key=True)
    ponderation = Column(String, nullable=False)


class enumIndicateur(Base):
    __tablename__ = "enum_indicateur"
    __table_args__ = {'schema': 'mesure_bruit'}
    id_indicateur = Column(Integer, Sequence('enum_indicateur_id_indicateur_seq', schema='mesure_bruit'), primary_key=True)
    indicateur = Column(String, nullable=False)