import os

from sqlalchemy import create_engine, text
from sqlalchemy.orm import sessionmaker

from .models import User, Base

USERNAME = os.getenv("POSTGRES_USER")
PASSWORD = os.getenv("POSTGRES_PASSWORD")
CONTAINER_NAME = "localhost" if __name__ == "__main__" else "db"
CONTAINER_PORT = 5432
DATABASE_NAME = os.getenv("POSTGRES_DB")

engine = create_engine(f'postgresql://{USERNAME}:{PASSWORD}@{CONTAINER_NAME}:{CONTAINER_PORT}/{DATABASE_NAME}')
Session = sessionmaker(bind=engine)
Base.metadata.create_all(engine)
