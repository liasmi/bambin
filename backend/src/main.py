from typing import Union

from fastapi import FastAPI, Request, Form
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi.middleware.cors import CORSMiddleware
from sqlalchemy import text, select


from src.db import Session
from src.models import User, MaterielMesure, enumTypeMateriel, enumMarqueMateriel
from src.pydantic_models import MandatoryFormInput
from src.exporter import Exporter
from src.whitelist import whitelist

app = FastAPI()
app.mount("/static", StaticFiles(directory="src/static"), name="static")
templates = Jinja2Templates(directory="src/templates")
# tutorial on Forms: https://eugeneyan.com/writing/how-to-set-up-html-app-with-fastapi-jinja-forms-templates/

app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:8080"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
async def welcome(request: Request):
    return "Welcome from the backend!"


@app.get("/users")
def get_users():
    with Session() as session:
        result = session.query(User).filter_by().all()
    return result


@app.get("/materiel_mesure")
def get_materiel():
    with Session() as session:
        result = session.execute(
            select(
                MaterielMesure.id_materiel_mesure,
                enumTypeMateriel.type_materiel,
                enumMarqueMateriel.marque_materiel,
                MaterielMesure.numero_serie,
            )
            .join(
                enumTypeMateriel,
                MaterielMesure.id_type_materiel == enumTypeMateriel.id_type_materiel,
            )
            .join(
                enumMarqueMateriel,
                MaterielMesure.id_marque_materiel == enumMarqueMateriel.id_marque_materiel,
                isouter=True,
            )
        ).all()
        resultDict = [
                        {
                            "id_materiel_mesure": b[0],
                            "type_materiel": b[1],
                            "marque_materiel": b[2],
                            "numero_serie": b[3],
                        }
                        for b in result
                    ]
    return resultDict


@app.post("/users")
def create_users(name, fullname, nickname):
    with Session() as session:
        session.add(User(name=name, fullname=fullname, nickname=nickname))
        session.commit()
        result = (
            session.query(User)
            .filter_by(role=name, fullname=fullname, nickname=nickname)
            .first()
        )
    return result


@app.get("/whitelist")
def get_whitelist():
    return whitelist


@app.get("/dbcontent")
def get_db_content(schema, table, field):
    if (schema, table, field) not in whitelist:
        raise Exception("Wrong schema or datable or field")
    with Session() as session:
        result = session.execute(text(f"SELECT {field} FROM {schema}.{table}")).all()
    return [r[0] for r in result]


@app.post(path="/submit", response_model=MandatoryFormInput)
def submitted_form(body: MandatoryFormInput):
    exporter = Exporter(Session)
    return exporter.export_to_databases(body)
