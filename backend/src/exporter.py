from src.models import MesureBruit, LocalisationMesure, ResultatMesureBruit
from src.pydantic_models import MandatoryFormInput
from src.importer import nextval_id_localisation_mesure, nextval_id_mesure_bruit


class Exporter:
    def __init__(self, session_instance):
        self.session_instance = session_instance

    def export_to_databases(self, body: MandatoryFormInput):
        

        # table localisation_mesure
        if body.id_localisation_mesure:
            id_loc = body.id_localisation_mesure
        else:
            id_loc = nextval_id_localisation_mesure(self.session_instance)
        localisation_mesure = LocalisationMesure(
            id_localisation_mesure=id_loc,
            geom=self.convert_coordinates(body.x_coordinate_epsg2154, 
                                         body.y_coordinate_epsg2154),
            commentaire=body.comment_localisation
        )
        self.base_export_to_databases(value=localisation_mesure)

        # Table mesure de bruit
        id_mesure_bruit = nextval_id_mesure_bruit(self.session_instance)
        mesure_bruit = MesureBruit(
            id_mesure_bruit=id_mesure_bruit,
            maitre_ouvrage=body.maitre_ouvrage,
            maitre_oeuvre=body.maitre_oeuvre,
            is_point_fixe=body.is_point_fixe,
            id_point_fixe=body.id_point_fixe,
            id_localisation_mesure=id_loc,
            is_exterieur=body.is_exterieur,
            is_facade=body.is_facade,
            is_champ=body.is_champ,
            hauteur_mesure=body.hauteur_mesure,
            id_piece_mesure=body.id_piece_mesure,
            id_piece_mesure_orient=body.id_piece_mesure_orient,
            is_ponctuel=body.is_ponctuel,
            date_debut=body.date_debut,
            date_fin=body.date_fin,
            duree_integration=body.duree_integration,
            id_materiel_mesure=body.id_materiel_mesure,
            id_rapport=body.id_rapport,
            id_campagne_mesure=body.id_campagne_mesure,
            id_meteo=body.id_meteo,
            commentaire=body.comment_mesure
        )
        self.base_export_to_databases(value=mesure_bruit)

        # Table resultat
        resultat_mesure = ResultatMesureBruit(
            id_mesure_bruit=id_mesure_bruit,
            id_nature_mesure=body.id_nature_mesure,
            id_indicateur=body.id_indicateur,
            id_unite_grandeur=body.id_unite_grandeur,
            id_ponderation=body.id_ponderation,
            debut_periode=body.debut_periode,
            fin_periode=body.fin_periode,
            is_periode_reference=body.is_periode_reference,
            valeur=body.valeur,
            incertitude=body.incertitude,
            commentaire=body.comment_resultat
        )


        return body

    def base_export_to_databases(self, value):
        with self.session_instance() as session:
            session.add(value)
            session.commit()

    def convert_coordinates(self, x, y):
        return f'SRID=2154;POINT({x} {y})'
        
